# Resurrect Kitten (git-rk)

>  **Every time you 'git commit -m "WIP"', a kitten dies somewhere!**

After you had your developer rampage and coded hundreds of files, touched thousands of lines, modified oceans of bits, you realized you made a mess with your commits. 

Now you are filled with "WIP" commits and don't remember what you did en each one!... I'm one of those! :(

If you are a kitten killer like me, then you should stop right now, and get tidy!. Seriously.

If you are still killing kittens and cannot stop, then this tool may be for you.

This tool splits your commits into a single commit per file, letting you reorder and organize your commits before you push to your community.

> **Every time you use this tool, a kitten crawles out of it's grave!**

## Install

```bash

$ git clone https://gitlab.com/thekesolutions/tools/git-rk.git

$ cd git-rk

$ sudo ln -s $(pwd)/git-rk /usr/local/bin/

```

## Use

```bash
git rk [options] <commit>
```

## Options

* __-i --interactive__: Interactive rebase before splitting files
* __--no-squash__: Do not squash, just split files for each commit. Commit message will be like '[file name]: [original commit message]'

## Example

If you want to split the last 5 commits, all you should do is

```bash
git rk HEAD~5
```

all your 5 commits will dissapear, and will be replaced by a nice commit per file touched in those killer commits.

After that, you've got a nice base to orginize your changes in a kitten friendly manner!.

## Real life example

### kitten killer log

I was working on a bug in my woking place and in home, and really had no time to save some kittens before switching from one place to an other.

This is the log

```bash
~/git/Koha(bug-19618)$ git log --oneline
9a9f6518ff (HEAD -> bug-19618, origin/bug-19618) WIP
04124e46ee WIP - add ClubHold classes
1c7b995b02 WIP
fd17a838a4 WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
3563a4f78d WIP - complete api for clubs hold
79646e779a WIP - add clubs api
f42eedb9b8 add clubs rest api
c9f0353ebf WIP - done request.tt now placerequest.pl
b482add839 WIP - starting updates to request.tt
464f64eff6 Koha::Club::Enrollment->is_canceled
28c6b7bb87 WIP - add clubs to request.pl
06b319e0f5 WIP - add 'holdforclub' cookie, and display in search results
e239047208 WIP - add option to clubs.tt
```

Aweful, I know!

### rk --no-squash

Ok, one commit per file may be not modular enough.. you can add --no-squash option and you'll get a log like this

```bash
~/git/Koha(bug-19618)$ git rk --no-squash HEAD~13

#You'll get a verbose output here

~/git/Koha(bug-19618)$ git log --oneline         
de38a6ccc1 (HEAD -> bug-19618) koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP
3d53213c43 api/v1/swagger/paths/clubs.json: WIP
847ece332e added api/v1/swagger/definitions/club_holds.json: WIP
9717a83ede added api/v1/swagger/definitions/club_hold_patron_holds.json: WIP
4c1b2d2277 added api/v1/swagger/definitions/club_hold_patron_hold.json: WIP
f3572a3284 added api/v1/swagger/definitions/club_hold.json: WIP
fb32d64b28 Koha/Club/Hold.pm: WIP
d8283fbd2c installer/data/mysql/kohastructure.sql: WIP - add ClubHold classes
83cc71aae4 added installer/data/mysql/atomicupdate/club_holds.perl: WIP - add ClubHold classes
f7de35b548 Koha/Schema/Result/Reserve.pm: WIP - add ClubHold classes
9c738bd041 Koha/Schema/Result/Item.pm: WIP - add ClubHold classes
70f10878f6 added Koha/Schema/Result/ClubHoldsToPatronHold.pm: WIP - add ClubHold classes
648b6245e3 added Koha/Schema/Result/ClubHold.pm: WIP - add ClubHold classes
34bb3f4287 Koha/Schema/Result/Club.pm: WIP - add ClubHold classes
c2de033dcb Koha/Schema/Result/Borrower.pm: WIP - add ClubHold classes
81ec71d92a Koha/Schema/Result/Biblio.pm: WIP - add ClubHold classes
8e7190f2cb Koha/REST/V1/Clubs/Holds.pm: WIP - add ClubHold classes
84acf7e2de added Koha/Exceptions/ClubHold.pm: WIP - add ClubHold classes
6058094387 added Koha/Club/Holds.pm: WIP - add ClubHold classes
3f54420344 added Koha/Club/Hold/PatronHolds.pm: WIP - add ClubHold classes
a0aa66abb9 added Koha/Club/Hold/PatronHold.pm: WIP - add ClubHold classes
2718dfba0f added Koha/Club/Hold.pm: WIP - add ClubHold classes
309ac3862b installer/data/mysql/kohastructure.sql: WIP
20a53fdd28 Koha/REST/V1/Clubs/Holds.pm: WIP
09ab6f46c3 koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
0aea2281c4 koha-tmpl/intranet-tmpl/prog/en/modules/clubs/clubs.tt: WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
19394ebffc added koha-tmpl/intranet-tmpl/prog/en/includes/clubs-table.inc: WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
a72080c6b6 Koha/REST/V1/Clubs/Holds.pm: WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
05643ea9ed Koha/Club.pm: WIP - add clubs-table.inc and revert Koha::Club::club_enrollemnts
7e627b7054 reserve/request.pl: WIP - complete api for clubs hold
fd982a0efa koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP - complete api for clubs hold
b94af46946 Koha/REST/V1/Clubs/Holds.pm: WIP - complete api for clubs hold
bdc60e955a Koha/Club.pm: WIP - complete api for clubs hold
8c3c695164 koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP - add clubs api
37124cce9e added api/v1/swagger/paths/clubs.json: WIP - add clubs api
6f1a8288ff api/v1/swagger/paths.json: WIP - add clubs api
028302b542 added api/v1/swagger/parameters/club.json: WIP - add clubs api
cf4759a4e2 api/v1/swagger/parameters.json: WIP - add clubs api
874594de46 added Koha/REST/V1/Clubs/Holds.pm: WIP - add clubs api
cc0aea7f2b removed Koha/REST/V1/Clubs.pm: WIP - add clubs api
ba2c8688e7 added Koha/REST/V1/Clubs.pm: add clubs rest api
3a34e554e8 reserve/request.pl: WIP - done request.tt now placerequest.pl
cf08de36b2 reserve/placerequest.pl: WIP - done request.tt now placerequest.pl
bea0951aca koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP - done request.tt now placerequest.pl
891c36401f reserve/request.pl: WIP - starting updates to request.tt
07ef4e7b4b koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt: WIP - starting updates to request.tt
452f356fb5 Koha/Club/Enrollment.pm: Koha::Club::Enrollment->is_canceled
4816bf6ee5 reserve/request.pl: WIP - add clubs to request.pl
0a71783f5a koha-tmpl/intranet-tmpl/prog/en/modules/members/moremember.tt: WIP - add clubs to request.pl
ac46b540c6 koha-tmpl/intranet-tmpl/prog/en/modules/clubs/clubs.tt: WIP - add 'holdforclub' cookie, and display in search results
dfcd09ad18 koha-tmpl/intranet-tmpl/prog/en/modules/catalogue/results.tt: WIP - add 'holdforclub' cookie, and display in search results
dbfea9240a catalogue/search.pl: WIP - add 'holdforclub' cookie, and display in search results
db7f5b9a11 koha-tmpl/intranet-tmpl/prog/en/modules/clubs/clubs.tt: WIP - add option to clubs.tt
```

Now every killer commit is splitted per every file it touched, giving you the chance to squash them orderly

### rk (squashing)

If you want a plain single commit per file, do not add --no-squash option, and you'll get a log like this

```bash
~/git/Koha(bug-19618)$ git rk HEAD~13

#You'll get a verbose output here

~/git/Koha(bug-19618)$ git log --oneline
d12deabdd5 (HEAD -> bug-19618) api/v1/swagger/definitions/club_holds.json
d55512d144 api/v1/swagger/definitions/club_hold_patron_holds.json
518880cdcc api/v1/swagger/definitions/club_hold_patron_hold.json
f7ac145476 api/v1/swagger/definitions/club_hold.json
e175c3eb6d installer/data/mysql/atomicupdate/club_holds.perl
a10bb27c63 Koha/Schema/Result/Reserve.pm
7c34468d0a Koha/Schema/Result/Item.pm
5453dc2682 Koha/Schema/Result/ClubHoldsToPatronHold.pm
935b2b80be Koha/Schema/Result/ClubHold.pm
5ae2abfebb Koha/Schema/Result/Club.pm
df074291c4 Koha/Schema/Result/Borrower.pm
63e3f29b13 Koha/Schema/Result/Biblio.pm
e124422269 Koha/Exceptions/ClubHold.pm
fd7c1a0c0b Koha/Club/Holds.pm
4981b38108 Koha/Club/Hold/PatronHolds.pm
67e840b85c Koha/Club/Hold/PatronHold.pm
7819b8965d Koha/Club/Hold.pm
da99608310 installer/data/mysql/kohastructure.sql
f85e7076fe koha-tmpl/intranet-tmpl/prog/en/includes/clubs-table.inc
6c2202dc56 Koha/Club.pm
fb4a751f73 api/v1/swagger/paths/clubs.json
156b7211dc api/v1/swagger/paths.json
bb83e05e92 api/v1/swagger/parameters/club.json
ca4623d8c1 api/v1/swagger/parameters.json
b76611be27 Koha/REST/V1/Clubs/Holds.pm
e5c5dadec0 removed Koha/REST/V1/Clubs.pm
112132bf62 Koha/REST/V1/Clubs.pm
475c0e3885 reserve/placerequest.pl
7ef90ce3a0 koha-tmpl/intranet-tmpl/prog/en/modules/reserve/request.tt
7cd25d31a1 Koha/Club/Enrollment.pm
d04e794146 reserve/request.pl
531bf9cfc5 koha-tmpl/intranet-tmpl/prog/en/modules/members/moremember.tt
36b0270fc3 koha-tmpl/intranet-tmpl/prog/en/modules/catalogue/results.tt
b86126faed catalogue/search.pl
d370fc4f03 koha-tmpl/intranet-tmpl/prog/en/modules/clubs/clubs.tt
```

Now, we can reorder commits, give them useful messages, and save kittens!

## Thanks

Special thanks to _hisham_ for writing this [article](https://hisham.hm/2019/02/12/splitting-a-git-commit-into-one-commit-per-file/).

Enjoy!